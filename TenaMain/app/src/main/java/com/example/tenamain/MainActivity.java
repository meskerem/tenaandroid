package com.example.tenamain;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {

    ProgressBar progressBar;
    int count = 0;
    Button share_btn;
    Button fund_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progress();
        fund_btn = (Button)findViewById(R.id.fund_btn);
        fund_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fund();

            }
        });

        share_btn = (Button)findViewById(R.id.share_btn);
        share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String shareBody = "please share to help the fundraiser reach more donation ";
                String shareSub = "your subject here";
                intent.putExtra(Intent.EXTRA_SUBJECT,shareSub);
                intent.putExtra(Intent.EXTRA_TEXT,shareBody);
                startActivity(Intent.createChooser(intent, "share using!!!"));
            }


        });
    }

    public void progress(){

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

    }
    public void fund(){
        Intent myIntent = new Intent(this, Fund.class);
        startActivity(myIntent);
    }
}